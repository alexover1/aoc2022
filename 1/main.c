#define INPUT_FILE "input.txt"
#include "../aoc.c"

uint64_t *array_least(uint64_t *array, size_t array_size)
{
    uint64_t *least = NULL;
    for (size_t i = 0; i < array_size; ++i) {
        if (!least) least = &array[i];
        else if (array[i] < *least) least = &array[i];
    }
    return least;
}

void part_one(String_View contents)
{
    uint64_t greatest = 0;
    uint64_t sum = 0;

    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        if (!line.count) {
            if (sum > greatest) greatest = sum;
            sum = 0;
            continue;
        }

        sum += sv_chop_u64(&line);
        assert(line.count == 0 && "The line contained more than a single number.");
    }

    printf("Part One: %lu\n", greatest);
}

#define PART_TWO_WINDOW_SIZE 3

void part_two(String_View contents)
{
    uint64_t window[PART_TWO_WINDOW_SIZE] = {0};
    uint64_t sum = 0;

    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        if (!line.count) {
            uint64_t *least = array_least(window, PART_TWO_WINDOW_SIZE);
            if (sum > *least) {
                *least = sum;
            }
            sum = 0;
            continue;
        }

        sum += sv_chop_u64(&line);
        assert(line.count == 0 && "The line contained more than a single number.");
    }

    printf("Part Two: %lu\n", window[0] + window[1] + window[2]);
}

void swap(uint64_t *a, uint64_t *b)
{
    uint64_t temp = *a;
    *a = *b;
    *b = temp;
}

void part_two_better(String_View contents)
{
    uint64_t a = 0, b = 0, c = 0;
    uint64_t sum = 0;

    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        if (!line.count) {
            if (sum > c) {
                c = sum;
                if (c > b) swap(&b, &c);
                if (b > a) swap(&a, &b);
            }

            sum = 0;
            continue;
        }

        sum += sv_chop_u64(&line);
        assert(line.count == 0 && "The line contained more than a single number.");
    }

    printf("Part Two Better: %lu\n", a + b + c);
}

int main()
{
    String_View input = read_input_file();
    part_one(input);
    part_two(input);
    part_two_better(input);
    return 0;
}
