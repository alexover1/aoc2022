#define INPUT_FILE "input.txt"
#include "../aoc.c"

void parse_range(String_View *s, uint64_t *start, uint64_t *end)
{
    *start = sv_chop_u64(s);
    assert(*s->data == '-');
    sv_chop_left(s, 1);
    *end = sv_chop_u64(s);
}

// Part 1: Range A is contained in range B if the start and end
// of A are less than or equal to the start and end of B.

void part_one(String_View contents)
{
    uint64_t count = 0;

    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        uint64_t start1, end1, start2, end2;
        parse_range(&line, &start1, &end1);

        assert(*line.data == ',');
        sv_chop_left(&line, 1);

        parse_range(&line, &start2, &end2);

        if ((start2 >= start1 && end2 <= end1) || (start1 >= start2 && end1 <= end2)) {
            count += 1;
        }
    }

    printf("Part One: %lu\n", count);
}

// Part 2: Two ranges overlap if the start of range A is contained in range B and vice-versa.

bool between(uint64_t x, uint64_t a, uint64_t b)
{
    return (x >= a) && (x <= b);
}

void part_two(String_View contents)
{
    uint64_t count = 0;

    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        uint64_t start1, end1, start2, end2;
        parse_range(&line, &start1, &end1);

        assert(*line.data == ',');
        sv_chop_left(&line, 1);

        parse_range(&line, &start2, &end2);

        if (between(start1, start2, end2) || between(start2, start1, end1)) {
            count += 1;
        }
    }

    printf("Part Two: %lu\n", count);
}

int main()
{
    String_View input = read_input_file();
    part_one(input);
    part_two(input);
    return 0;
}
