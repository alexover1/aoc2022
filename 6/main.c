#define INPUT_FILE "input.txt"
#include "../aoc.c"

bool sv_unique_naive(String_View s)
{
    for (size_t i = 0; i < s.count; ++i) {
        for (size_t j = i + 1; j < s.count; ++j) {
            if (s.data[i] == s.data[j]) {
                return false;
            }
        }
    }
    return true;
}

bool sv_unique_mask(String_View s, char start, char end)
{
    assert((end - start) <= 64);

    uint64_t mask = 0;

    for (size_t i = 0; i < s.count; ++i) {
        assert(s.data[i] >= start && s.data[i] <= end);

        uint64_t c = 1 << (s.data[i] - start);

        if ((mask & c) > 0) {
            return false;
        }

        mask |= c;
    }
    return true;
}

void part_one(String_View contents, size_t window_size)
{
    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');
        assert(line.count >= window_size);

        for (size_t i = 0; i < line.count; ++i) {
            if (sv_unique_mask(sv_from_parts(line.data + i, window_size), 'A', 'z')) {
                printf("%zu\n", i + window_size);
                break;
            }
        }
    }
}

int main()
{
    String_View input = read_input_file();
    part_one(input, 4);
    part_one(input, 14);
    return 0;
}
