#define INPUT_FILE "input.txt"
#include "../aoc.c"

typedef enum {
    LOSE = 0,
    DRAW = 3,
    WIN = 6,
} Outcome;

// These are equal to the score you get for using them.
typedef enum {
    ROCK = 1,
    PAPER = 2,
    SCISSORS = 3
} Shape;

Shape shape_wins_against[4] = {
    [ROCK] = SCISSORS,
    [PAPER] = ROCK,
    [SCISSORS] = PAPER,
};

Shape shape_loses_to[4] = {
    [ROCK] = PAPER,
    [PAPER] = SCISSORS,
    [SCISSORS] = ROCK,
};

Shape parse_shape(char ch)
{
    if      (ch == 'A' || ch == 'X') return ROCK;
    else if (ch == 'B' || ch == 'Y') return PAPER;
    else if (ch == 'C' || ch == 'Z') return SCISSORS;
    else assert(false);
}

Outcome parse_outcome(char ch)
{
    if      (ch == 'X') return LOSE;
    else if (ch == 'Y') return DRAW;
    else if (ch == 'Z') return WIN;
    else assert(false);
}

Outcome play_game(Shape theirs, Shape yours)
{
    if (theirs == yours) return DRAW;
    return (theirs == shape_wins_against[yours]) ? WIN : LOSE;
}

void part_one(String_View contents)
{
    uint64_t score = 0;

    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        Shape theirs = parse_shape(*line.data);
        line.data += 2;
        Shape yours = parse_shape(*line.data);

        score += play_game(theirs, yours) + yours;
    }

    printf("Part One: %lu\n", score);
}

void part_two(String_View contents)
{
    uint64_t score = 0;

    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        Shape theirs = parse_shape(*line.data);
        line.data += 2;
        Outcome outcome = parse_outcome(*line.data);

        Shape yours;

        if (outcome == DRAW) yours = theirs;
        else if (outcome == LOSE) yours = shape_wins_against[theirs];
        else if (outcome == WIN) yours = shape_loses_to[theirs];
        else assert(false);

        score += play_game(theirs, yours) + yours;
    }

    printf("Part Two: %lu\n", score);
}

int main()
{
    String_View input = read_input_file();
    part_one(input);
    part_two(input);
    return 0;
}
