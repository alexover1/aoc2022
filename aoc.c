#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <inttypes.h>
#include <errno.h>
#include <string.h>

#define SV_IMPLEMENTATION
#include "./third_party/sv.h"

typedef const char *Cstr;
typedef int Errno;

#define return_defer(value) do { result = (value); goto defer; } while(0);

Errno read_entire_file(Cstr filename, char **buffer, size_t *buffer_size)
{
    Errno result = 0;
    FILE *f = NULL;

    f = fopen(filename, "rb");
    if (f == NULL) return_defer(errno);

    if (fseek(f, 0, SEEK_END) < 0) return_defer(errno);
    long m = ftell(f);
    if (m < 0) return_defer(errno);
    if (fseek(f, 0, SEEK_SET) < 0) return_defer(errno);

    *buffer_size = m;
    *buffer = malloc(m + 1);
    (*buffer)[*buffer_size] = '\0';

    fread(*buffer, *buffer_size, 1, f);
    if (ferror(f)) return_defer(errno);

defer:
    if (f) fclose(f);
    return result;
}

String_View read_input_file(void)
{
    char *buffer = NULL;
    size_t buffer_size;

    Errno err = read_entire_file(INPUT_FILE, &buffer, &buffer_size);
    if (err != 0) {
        fprintf(stderr, "Error: Failed to read file '%s': %s\n", INPUT_FILE, strerror(errno));
        exit(1);
    }

    return sv_from_parts(buffer, buffer_size);
}
