#define INPUT_FILE "input.txt"
#include "../aoc.c"

#define STB_DS_IMPLEMENTATION
#include "../third_party/stb_ds.h"

typedef struct File File;

struct File {
    String_View name;
    uint64_t size;
    File *parent;
    File **children; // If a directory.
    bool visited;
};

#define FILE_IS_DIRECTORY(file) ((file)->children != NULL)

#define FILE_ARENA_CAPACITY 2048
File file_arena[2048];
size_t file_arena_size = 0;

File *make_file(String_View name, uint64_t size, File *parent)
{
    assert(file_arena_size < FILE_ARENA_CAPACITY);
    File *file = &file_arena[file_arena_size];
    file_arena_size += 1;

    file->name = name;
    file->size = size;
    file->parent = parent;
    file->children = NULL;
    file->visited = false;
    return file;
}

File *find_file_in_directory(File *dir, String_View name)
{
    for (size_t i = 0; i < arrlenu(dir->children); ++i) {
        if (sv_eq(dir->children[i]->name, name)) {
            return dir->children[i];
        }
    }
    return NULL;
}

uint64_t size_of_file_system(File *file)
{
    if (file->children == NULL || file->visited) return file->size;

    uint64_t sum = 0;
    for (size_t i = 0; i < arrlenu(file->children); ++i) {
        sum += size_of_file_system(file->children[i]);
    }

    // Mark the file as visited.
    file->visited = true;
    file->size = sum;

    return sum;
}

void print_file_system(File *file, size_t level)
{
    for (size_t i = 0; i < level; ++i) {
        printf("  ");
    }
    printf(SV_Fmt"\n", SV_Arg(file->name));

    for (int i = 0; i < arrlen(file->children); ++i) {
        print_file_system(file->children[i], level + 1);
    }
}

void parse_input(String_View contents, File *root)
{
    File *cwd = root;
    bool parsing_ls_output = false;

    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');
        assert(line.count);

        if (*line.data == '$') {
            sv_chop_left(&line, 2); // Skip the '$ '.

            parsing_ls_output = false;

            String_View command = sv_chop_by_delim(&line, ' ');

            if (sv_eq(command, sv_from_cstr("cd"))) {
                String_View path = sv_chop_by_delim(&line, ' ');

                if (sv_eq(path, sv_from_cstr("/"))) {
                    cwd = root;
                }
                else if (sv_eq(path, sv_from_cstr(".."))) {
                    assert(cwd->parent != NULL);
                    cwd = cwd->parent;
                }
                else {
                    cwd = find_file_in_directory(cwd, path);
                    assert(cwd);
                }
            }
            else if (sv_eq(command, sv_from_cstr("ls"))) {
                parsing_ls_output = true;
            }
            else {
                assert(false);
            }
        }
        else {
            assert(parsing_ls_output);

            String_View word = sv_chop_by_delim(&line, ' ');

            if (sv_eq(word, sv_from_cstr("dir"))) {
                arrput(cwd->children, make_file(line, 0, cwd));
            }
            else {
                assert(isdigit(*word.data));
                uint64_t size = sv_to_u64(word);
                arrput(cwd->children, make_file(line, size, cwd));
            }
        }
    }
}

void find_deletion_candidates(File *file, uint64_t *total)
{
    for (int i = 0; i < arrlen(file->children); ++i) {
        if (file->children[i]->children == NULL) continue; // Skip files (leaves).

        uint64_t size = size_of_file_system(file->children[i]);
        if (size <= 100000) *total += size;

        find_deletion_candidates(file->children[i], total);
    }
}

void part_one(File *root)
{
    uint64_t total = 0;
    find_deletion_candidates(root, &total);
    printf("Part One: %lu\n", total);
}

#define TOTAL_DISK_SPACE 70000000
#define UPDATE_SIZE 30000000

void part_two(File *root)
{
    uint64_t used_space = size_of_file_system(root);
    uint64_t unused_space = TOTAL_DISK_SPACE - used_space;

    if (unused_space < UPDATE_SIZE) {
        uint64_t needed_space = UPDATE_SIZE - unused_space;

        File *smallest_dir_that_frees_enough_space = root;

        // Find all directories.
        for (size_t i = 0; i < file_arena_size; ++i) {
            File *file = &file_arena[i];
            if (!FILE_IS_DIRECTORY(file)) continue;

            assert(file->visited); // Part 1.

            if (file->size >= needed_space) {
                if (file->size < smallest_dir_that_frees_enough_space->size) {
                    smallest_dir_that_frees_enough_space = file;
                }
            }
        }

        printf("Part Two: %lu\n", smallest_dir_that_frees_enough_space->size);
    }
}

int main()
{
    String_View input = read_input_file();

    File *root = make_file(sv_from_cstr("/"), 0, NULL);
    parse_input(input, root);

    part_one(root);
    part_two(root);
    return 0;
}
