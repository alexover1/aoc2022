#define INPUT_FILE "input.txt"
#include "../aoc.c"

#define STB_DS_IMPLEMENTATION
#include "../third_party/stb_ds.h"

typedef enum {
    EXPRESSION_OLD,
    EXPRESSION_INTEGER,
} Expression_Kind;

typedef struct {
    Expression_Kind kind;
    int value;
} Expression;

typedef struct {
    Expression lhs;
    Expression rhs;
    char op;
} Operation;

uint8_t TEST_DIVISIBLE_BY = 1;

typedef size_t Monkey_Id;

typedef struct {
    uint8_t kind;
    int operand;

    Monkey_Id true_branch;
    Monkey_Id false_branch;
} Test;

typedef struct {
    int *items;
    Operation operation;
    Test test;
    size_t number_of_inspections;
} Monkey;

Monkey *monkies = NULL;

Expression parse_expression(String_View *line)
{
    Expression result;

    String_View word = sv_chop_by_delim(line, ' ');

    if (sv_eq(word, sv_from_cstr("old"))) {
        result.kind = EXPRESSION_OLD;
    }
    else {
        result.kind = EXPRESSION_INTEGER;
        result.value = (int)sv_to_u64(word);
    }

    return result;
}

void parse_input(String_View contents)
{
    while (contents.count) {
        String_View line = sv_trim(sv_chop_by_delim(&contents, '\n'));

        if (!line.count) continue;

        String_View word = sv_chop_by_delim(&line, ' ');
        assert(sv_eq(word, sv_from_cstr("Monkey")));

        Monkey monkey = {0};

        // Staring items

        line = sv_trim(sv_chop_by_delim(&contents, '\n'));
        sv_chop_by_sv(&line, sv_from_cstr("Starting items: "));

        arrput(monkey.items, sv_chop_u64(&line));

        while (sv_starts_with(line, sv_from_cstr(", "))) {
            sv_chop_left(&line, 2);
            arrput(monkey.items, sv_chop_u64(&line));
        }

        // Operation

        {
            line = sv_trim(sv_chop_by_delim(&contents, '\n'));
            sv_chop_by_sv(&line, sv_from_cstr("Operation: new = "));

            monkey.operation.lhs = parse_expression(&line);

            assert(line.count);
            monkey.operation.op = *line.data;
            sv_chop_left(&line, 2);

            monkey.operation.rhs = parse_expression(&line);
        }

        // Test

        {
            line = sv_trim(sv_chop_by_delim(&contents, '\n'));
            sv_chop_by_sv(&line, sv_from_cstr("Test: divisible by "));
            monkey.test.kind = TEST_DIVISIBLE_BY;
            monkey.test.operand = (int)sv_chop_u64(&line);

            line = sv_trim(sv_chop_by_delim(&contents, '\n'));
            sv_chop_by_sv(&line, sv_from_cstr("If true: throw to monkey "));
            monkey.test.true_branch = (size_t)sv_chop_u64(&line);

            line = sv_trim(sv_chop_by_delim(&contents, '\n'));
            sv_chop_by_sv(&line, sv_from_cstr("If false: throw to monkey "));
            monkey.test.false_branch = (size_t)sv_chop_u64(&line);
        }

        // Add the monkey.

        arrput(monkies, monkey);
    }
}

#define SWAP(T, a, b) do { \
    T temp = *(a);         \
    *(a) = *(b);           \
    *(b) = temp;           \
} while(0);

void part_one()
{
    size_t number_of_rounds = 20;

    // For all of the rounds...
    for (size_t round = 0; round < number_of_rounds; ++round) {
        // For all of the monkies...
        for (size_t id = 0; id < arrlenu(monkies); ++id) {
            Monkey *monkey = &monkies[id];

            assert(monkey->operation.lhs.kind == EXPRESSION_OLD);
            assert(monkey->test.kind == TEST_DIVISIBLE_BY);

            // For all of its items...
            for (size_t i = 0; i < arrlenu(monkey->items); ++i) {
                int rhs = (monkey->operation.rhs.kind == EXPRESSION_INTEGER) ? monkey->operation.rhs.value : monkey->items[i];

                if (monkey->operation.op == '+') {
                    monkey->items[i] += rhs;
                }
                else if (monkey->operation.op == '*') {
                    monkey->items[i] *= rhs;
                }
                else {
                    assert(false);
                }

                // The monkey gets bored. Decrease the worry level.
                monkey->items[i] /= 3;

                // Throw the item.
                Monkey_Id throw_to = ((monkey->items[i] % monkey->test.operand) == 0) ? monkey->test.true_branch : monkey->test.false_branch;
                arrput(monkies[throw_to].items, monkey->items[i]);

                // Increment inspection counter.
                monkey->number_of_inspections += 1;
            }

            // Now the monkey is holding nothing.
            arrfree(monkey->items);
            monkey->items = NULL;
        }
    }

    size_t a = 0, b = 0;

    for (size_t i = 0; i < arrlenu(monkies); ++i) {
        if (monkies[i].number_of_inspections > b) {
            b = monkies[i].number_of_inspections;
            if (b > a) SWAP(size_t, &a, &b);
        }
    }

    printf("Part One: %zu\n", a * b);
}

int main()
{
    parse_input(read_input_file());
    part_one();
    return 0;
}
