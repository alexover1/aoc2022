#define INPUT_FILE "input.txt"
#include "../aoc.c"

uint64_t priority(char c)
{
    if (c >= 'a' && c <= 'z') return c - 'a' + 1;

    assert(c >= 'A' && c <= 'Z');
    return c - 'A' + 27;
}

void part_one(String_View contents)
{
    uint64_t sum = 0;

    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');
        assert(line.count % 2 == 0);
        String_View first = sv_chop_left(&line, line.count/2);

        for (size_t i = 0; i < first.count; ++i) {
            char c = first.data[i];
            size_t index;
            if (sv_index_of(line, c, &index)) {
                sum += priority(c);
                break;
            }
        }
    }

    printf("Part One: %lu\n", sum);
}

void part_two(String_View contents)
{
        uint64_t sum = 0;

    while (contents.count) {
        String_View first = sv_chop_by_delim(&contents, '\n');
        String_View second = sv_chop_by_delim(&contents, '\n');
        String_View third = sv_chop_by_delim(&contents, '\n');

        for (size_t i = 0; i < first.count; ++i) {
            char c = first.data[i];
            size_t index;
            if (sv_index_of(second, c, &index)) {
                if (sv_index_of(third, c, &index)) {
                    sum += priority(first.data[i]);
                    break;
                }
            }
        }
    }

    printf("Part Two: %lu\n", sum);
}

int main()
{
    String_View input = read_input_file();
    part_one(input);
    part_two(input);
    return 0;
}
