#define INPUT_FILE "input.txt"
#include "../aoc.c"

#define WIDTH 100
#define HEIGHT 100

uint8_t forest[HEIGHT][WIDTH];
size_t width, height;

bool visible[HEIGHT][WIDTH];

void parse_input(String_View contents)
{
    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        if (width == 0) width = line.count;

        for (size_t i = 0; i < width; ++i) {
            forest[height][i] = line.data[i] - '0';
        }

        height += 1;
    }
}

void part_one(void)
{
    assert(width >= 2);
    assert(height >= 2);

    uint64_t count = 0;

    // Left and right
    for (size_t y = 1; y < height-1; ++y) {
        uint64_t left = forest[y][0], right = forest[y][width-1];

        for (size_t x = 1; x < width-1; ++x) {
            if (forest[y][x] > left) {
                left = forest[y][x];
                if (!visible[y][x]) {
                    count += 1;
                    visible[y][x] = true;
                }
            }

            if (forest[y][width-1 - x] > right) {
                right = forest[y][width-1 - x];
                if (!visible[y][width-1 - x]) {
                    count += 1;
                    visible[y][width-1 - x] = true;
                }
            }
        }
    }

    // Top and bottom
    for (size_t x = 1; x < width-1; ++x) {
        uint64_t top = forest[0][x], bottom = forest[width-1][x];

        for (size_t y = 1; y < height-1; ++y) {
            if (forest[y][x] > top) {
                top = forest[y][x];
                if (!visible[y][x]) {
                    count += 1;
                    visible[y][x] = true;
                }
            }

            if (forest[height-1 - y][x] > bottom) {
                bottom = forest[height-1 - y][x];
                if (!visible[height-1 - y][x]) {
                    count += 1;
                    visible[height-1 - y][x] = true;
                }
            }
        }
    }

    printf("Part One: %lu\n", count + 2*width + 2*height - 4);
}

size_t calculate_scenic_score(size_t x0, size_t y0)
{
    // Check the left
    size_t left = 0;
    for (int x = x0-1; x >= 0; --x) {
        left += 1;
        if (forest[y0][x] >= forest[y0][x0]) {
            break;
        }
    }

    // Check the right
    size_t right = 0;
    for (size_t x = x0+1; x < width; ++x) {
        right += 1;
        if (forest[y0][x] >= forest[y0][x0]) {
            break;
        }
    }

    // Check above
    size_t up = 0;
    for (int y = y0-1; y >= 0; --y) {
        up += 1;
        if (forest[y][x0] >= forest[y0][x0]) {
            break;
        }
    }

    // Check below
    size_t down = 0;
    for (size_t y = y0+1; y < height; ++y) {
        down += 1;
        if (forest[y][x0] >= forest[y0][x0]) {
            break;
        }
    }

    return left * right * up * down;
}

void part_two(void)
{
    size_t greatest = 0;

    for (size_t y = 0; y < height; ++y) {
        for (size_t x = 0; x < height; ++x) {
            size_t score = calculate_scenic_score(x, y);
            if (score > greatest) greatest = score;
        }
    }

    printf("Part Two: %lu\n", greatest);
}

int main()
{
    parse_input(read_input_file());
    part_one();
    part_two();
    return 0;
}
