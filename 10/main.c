#define INPUT_FILE "input.txt"
#include "../aoc.c"

#define STB_DS_IMPLEMENTATION
#include "../third_party/stb_ds.h"

typedef enum {
    ADDX,
    NOOP
} Instruction_Kind;

typedef struct {
    Instruction_Kind kind;
    int operand;
} Instruction;

typedef struct {
    int cycle;
    int X;
} Sugar_Processing_Unit;

Instruction make_instruction(Instruction_Kind kind, int operand)
{
    Instruction instruction;
    instruction.kind = kind;
    instruction.operand = operand;
    return instruction;
}

Instruction *instructions = NULL;

void parse_input(String_View contents)
{
    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        if (sv_starts_with(line, sv_from_cstr("addx"))) {
            sv_chop_by_sv(&line, sv_from_cstr("addx "));
            char s[32];
            memcpy(s, line.data, line.count);
            s[line.count] = '\0';
            int V = atoi(s);
            arrput(instructions, make_instruction(ADDX, V));
        }
        else {
            assert(sv_starts_with(line, sv_from_cstr("noop")));
            arrput(instructions, make_instruction(NOOP, 0));
        }
    }
}

int spu_cycle(Sugar_Processing_Unit *spu)
{
    spu->cycle += 1;
    if (spu->cycle < 20) return 0;

    if ((spu->cycle - 20)%40 == 0) {
        return spu->cycle * spu->X;
    }

    return 0;
}

void part_one(void)
{
    Sugar_Processing_Unit spu;
    spu.cycle = 0;
    spu.X = 1;

    int64_t sum = 0;

    for (size_t i = 0; i < arrlenu(instructions); ++i) {
        sum += spu_cycle(&spu);

        if (instructions[i].kind == ADDX) {
            sum += spu_cycle(&spu);
            spu.X += instructions[i].operand;
        }
    }

    printf("Part One: %ld\n", sum);
}

#define CRT_WIDTH 40
#define CRT_HEIGHT 6

typedef int Clock;
typedef char Pixel;
typedef int CRT;

typedef struct {
    Clock clock;
    Sugar_Processing_Unit spu;
    Pixel screen[CRT_WIDTH * CRT_HEIGHT];
    CRT cursor;
} ELF_II;

void cycle_clock(ELF_II *pc)
{
    pc->clock += 1;

    if (pc->spu.X < 0) {
        pc->screen[pc->cursor] = '.';
    } else {
        int left = pc->spu.X - 1;
        int right = pc->spu.X + 1;

        if (left < 0) left = pc->spu.X;

        // If we are currently drawing the sprite.
        int x = pc->cursor%CRT_WIDTH;

        if (x >= left && x <= right) {
            pc->screen[pc->cursor] = '#';
        } else {
            pc->screen[pc->cursor] = '.';
        }
    }

    pc->cursor += 1;
}

void run_instruction(ELF_II *pc, Instruction instruction)
{
    if (instruction.kind == ADDX) {
        cycle_clock(pc);
        pc->spu.X += instruction.operand;
    }
}

void part_two()
{
    ELF_II pc = {0};
    pc.spu.X = 1;

    memset(pc.screen, ' ', CRT_WIDTH * CRT_HEIGHT);

    for (size_t i = 0; i < arrlenu(instructions); ++i) {
        cycle_clock(&pc);
        run_instruction(&pc, instructions[i]);
    }

    printf("Part Two:\n");
    for (size_t y = 0; y < CRT_HEIGHT; ++y) {
        for (size_t x = 0; x < CRT_WIDTH; ++x) {
            printf("%c", pc.screen[y*CRT_WIDTH + x]);
        }
        printf("\n");
    }
}

int main()
{
    parse_input(read_input_file());
    part_one();
    part_two();
    return 0;
}
