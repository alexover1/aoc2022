#define INPUT_FILE "input.txt"
#include "../aoc.c"

typedef struct Crate Crate;

struct Crate {
    char name;
    Crate *below;
};

typedef struct {
    Crate *top;
} Pile;

Crate *make_crate(char name, Crate *below)
{
    Crate *crate = (Crate *)malloc(sizeof(Crate));
    crate->name = name;
    crate->below = below;
    return crate;
}

#define PILES_COUNT 9

void part_one(String_View contents)
{
    Pile piles[PILES_COUNT] = {0};

    bool finished_parsing_crates = false;

    while (contents.count && !finished_parsing_crates) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        if (sv_starts_with(line, sv_from_cstr(" 1"))) {
            // Expect an empty line.
            line = sv_chop_by_delim(&contents, '\n');
            assert(!line.count);

            finished_parsing_crates = true;
            break;
        }

        for (size_t i = 0; line.count; ++i) {
            String_View crate = sv_trim(sv_chop_left(&line, 4));

            if (crate.count) {
                sv_chop_left(&crate, 1); // Skip the [.

                Crate *new_crate = make_crate(*crate.data, NULL);

                Crate **crate = &piles[i].top;
                while (*crate != NULL) crate = &(*crate)->below;
                *crate = new_crate;
            }
        }
    }

    assert(finished_parsing_crates);

    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        sv_chop_by_delim(&line, ' '); // move
        uint64_t amount = sv_chop_u64(&line);
        sv_chop_left(&line, 1); // space
        sv_chop_by_delim(&line, ' '); // from
        uint64_t from = sv_chop_u64(&line);
        sv_chop_left(&line, 1); // space
        sv_chop_by_delim(&line, ' '); // to
        uint64_t to = sv_chop_u64(&line);

        assert(piles[from-1].top != NULL && "The pile is empty.");

        for (size_t i = 0; i < amount; ++i) {
            Crate *crate_being_moved = piles[from-1].top;

            piles[from-1].top = crate_being_moved->below;

            crate_being_moved->below = piles[to-1].top;
            piles[to-1].top = crate_being_moved;
        }
    }

    printf("Part One: ");
    for (size_t i = 0; i < PILES_COUNT; ++i) {
        printf("%c", piles[i].top != NULL ? piles[i].top->name : ' ');
    }
    printf("\n");
}

void part_two(String_View contents)
{
    Pile piles[PILES_COUNT] = {0};

    bool finished_parsing_crates = false;

    while (contents.count && !finished_parsing_crates) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        if (sv_starts_with(line, sv_from_cstr(" 1"))) {
            // Expect an empty line.
            line = sv_chop_by_delim(&contents, '\n');
            assert(!line.count);

            finished_parsing_crates = true;
            break;
        }

        for (size_t i = 0; line.count; ++i) {
            String_View crate = sv_trim(sv_chop_left(&line, 4));

            if (crate.count) {
                sv_chop_left(&crate, 1); // Skip the [.

                Crate *new_crate = make_crate(*crate.data, NULL);

                Crate **crate = &piles[i].top;
                while (*crate != NULL) crate = &(*crate)->below;
                *crate = new_crate;
            }
        }
    }

    assert(finished_parsing_crates);

    while (contents.count) {
        String_View line = sv_chop_by_delim(&contents, '\n');

        sv_chop_by_delim(&line, ' '); // move
        uint64_t amount = sv_chop_u64(&line);
        sv_chop_left(&line, 1); // space
        sv_chop_by_delim(&line, ' '); // from
        uint64_t from = sv_chop_u64(&line);
        sv_chop_left(&line, 1); // space
        sv_chop_by_delim(&line, ' '); // to
        uint64_t to = sv_chop_u64(&line);

        // :swag: Didn't even need a doubly linked list.

        assert(piles[from-1].top != NULL && "The pile is empty.");

        Crate *top_crate_being_moved = piles[from-1].top;

        // Find the bottom.
        Crate *bottom_crate_being_moved = piles[from-1].top;
        for (size_t i = 1; i < amount; ++i) bottom_crate_being_moved = bottom_crate_being_moved->below;

        assert(bottom_crate_being_moved != NULL);

        piles[from-1].top = bottom_crate_being_moved->below;

        bottom_crate_being_moved->below = piles[to-1].top;
        piles[to-1].top = top_crate_being_moved;
    }

    printf("Part Two: ");
    for (size_t i = 0; i < PILES_COUNT; ++i) {
        printf("%c", piles[i].top != NULL ? piles[i].top->name : ' ');
    }
    printf("\n");
}

int main()
{
    String_View input = read_input_file();
    part_one(input);
    part_two(input);
    return 0;
}
